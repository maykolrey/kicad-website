+++
title = "An Open Source Thermal Imaging IR Camera"
projectdeveloper = "CircuitDigest"
projecturl = "https://circuitdigest.com/microcontroller-projects/diy-thermal-camera-using-esp32-wrover-module-and-mlx90640-sensor"
"made-with-kicad/categories" = [
    "Instrumentation"
]
+++

With an aim to make Thermal Camera affortable for hobbyists and makers
we built this DIY thermal camera using an ESP32 microcontroller and an
MLX90640 thermal sensor. The camera captures thermal images with a
resolution of 32x24 pixels and features a 2.4-inch display for
visualization. It offers adjustable refresh rates, various color
palettes, and temperature measurement capabilities between -40°C and
300°C.
